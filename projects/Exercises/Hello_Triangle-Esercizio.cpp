#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void process_input(GLFWwindow* window);

const unsigned int WIDTH = 800;
const unsigned int HEIGHT = 600;

float tri_1[] = {
	//first triangle
	-0.9f, -0.5f, 0.0f,  // left 
	-0.0f, -0.5f, 0.0f,  // right
	-0.45f, 0.5f, 0.0f,  // top
};
float tri_2[] = {
	//second triangle
	 0.0f, -0.5f, 0.0f,  // left
	 0.9f, -0.5f, 0.0f,  // right
	 0.45f, 0.5f, 0.0f   // top 
};

int main() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//- GLFW window creation
	//----------------------
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to initialize GLAD" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	//- Init GLAD
	//-----------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {

		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	//- Vertex Buffer Object and Vertex Array Object
	//---------------
	unsigned int VBO_1, VBO_2;
	glGenBuffers(1, &VBO_1);
	glGenBuffers(1, &VBO_2);

	unsigned int VAO_1, VAO_2;
	//triangle 1
	glGenVertexArrays(1, &VAO_1);
	glBindVertexArray(VAO_1);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tri_1), tri_1, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	//triangle 2
	glGenVertexArrays(1, &VAO_2);
	glBindVertexArray(VAO_2);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tri_2), tri_2, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	//- Vertex Shader
	//---------------
	const char* VertexShaderSource = "#version 330 core\n"
		"layout (location = 0) in vec3 aPos;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
		"}\0";

	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &VertexShaderSource, NULL);
	glCompileShader(vertexShader);

	//check for vertexShader compiling
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	//- Fragment shaders
	//------------------
	const char* FragmentShaderSource_O = "#version 330 core\n"
		"out vec4 FragColor;\n"
		"void main()\n"
		"{\n"
		"   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
		"}\n\0";
	const char* FragmentShaderSource_Y = "#version 330 core\n"
		"out vec4 FragColor;\n"
		"void main()\n"
		"{\n"
		"   FragColor = vec4(0.9f, 0.9f, 0.1f, 1.0f);\n"
		"}\n\0";

	unsigned int fragmentShader_O = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader_O, 1, &FragmentShaderSource_O, NULL);
	glCompileShader(fragmentShader_O);

	unsigned int fragmentShader_Y = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader_Y, 1, &FragmentShaderSource_Y, NULL);
	glCompileShader(fragmentShader_Y);

	//check for fragmentShader compiling
	glGetShaderiv(fragmentShader_O, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader_O, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	//- Shader programs (Orange and Yellow)
	//-------------------------------------------
	unsigned int shaderProgram_O = glCreateProgram();
	glAttachShader(shaderProgram_O, vertexShader);
	glAttachShader(shaderProgram_O, fragmentShader_O);
	glLinkProgram(shaderProgram_O); //linking

	unsigned int shaderProgram_Y = glCreateProgram();
	glAttachShader(shaderProgram_Y, vertexShader);
	glAttachShader(shaderProgram_Y, fragmentShader_Y);
	glLinkProgram(shaderProgram_Y); //linking

	//check for linking outcome
	glGetProgramiv(shaderProgram_O, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram_O, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}

	//deleting shaders after linking
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader_O);
	glDeleteShader(fragmentShader_Y);

	//- Render loop
	//-------------
	while (!glfwWindowShouldClose(window)) {

		//input
		process_input(window);

		//commands
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//use program previously created
		glUseProgram(shaderProgram_O);

		//bind VAO and draw triangle 1
		glBindVertexArray(VAO_1);
		glDrawArrays(GL_TRIANGLES, 0, 3);

		//change shader program
		glUseProgram(shaderProgram_Y);

		//bind VAO and draw triangle 2
		glBindVertexArray(VAO_2);
		glDrawArrays(GL_TRIANGLES, 0, 3);

		//events and buffers swap
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO_1);
	glDeleteBuffers(1, &VBO_1);
	glDeleteVertexArrays(1, &VAO_2);
	glDeleteBuffers(1, &VBO_2);

	glfwTerminate();
	return 0;
}
//=== End of main() ===


//=========================================


//- Function to reset the viewport on window resizing
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

//- Function to process input
void process_input(GLFWwindow* window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
}
